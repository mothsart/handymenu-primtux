# Changelog

Tous les changements importants sont documentés dans ce fichier à partir de la version 1.2.4

Le format est basé sur l'excellent : [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et le projet adhère à la nomenclature de version : [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased : 2.0]
### Added

- une interface d'édition la plus proche du handymenu final :
    * menu horizontal et non vertical
    * petit bouton intégré (une croix) à l'onglet de la section pour le supprimer
    * des flèches à gauche et à droite du menu pour déplacer l'onglet de section sélectionné 
    * le double-clic sur une section permet de la rééditer (un nom vide étant interdit, si il y a perte de focus on redonne le nom précédent)
    * un bouton d'ajout de section crée un onglet vide avec le focus pour l'éditer. (si on ne lui donne aucun nom, elle disparait)
- plus aucune action ne devrait rafraichir l'interface. (réinitialiser par exemple)
- éviter d'ouvrir une fenêtre à l'édition, 
    * un clic fait apparaitre : 
      un bouton de suppression en haut à gauche et la possibilité d'éditer le nom
    * le double-clic ouvre la boite de dialogue de changement d'icône
- garder un historique de modification et permettre "annuler/rétablir"
- rajouter des raccourcis clavier pour les utilisateurs expérimentés (par exemple, les entreprises qui installent primtux dans les écoles)
- http://forum.primtux.fr/viewtopic.php?pid=15063#10
- passage en GTK 3
- mise en place de tests unitaires et fonctionnels
- Prise en compte de la modification de l'apparence avec lxappearence => http://forum.primtux.fr/viewtopic.php?id=1547

## [Unreleased : 1.6.0]
### Added

- édition du logo du handymenu : principalement dans le cadre des handymenus personnalisés ou de l'ajout d'utilisateurs
- Pouvoir glisser une application directement dans la zone
- l'ajout d'une application propose un dialogue intermédiaire lorsque l'application existe déjà dans le handymenus du style : 
"cette application existe déjà dans la section xxx : désirez-vous l'avoir en doublon ?"
- la croix de fermeture du handymenu-mini pourrait être plus gros.
- ajouter des images à toutes les sections du handymenu-mini : vu que la cible ne sait pas ou peu lire.

## [Unreleased : 1.4.x]
### Fixed

- Le drag and drop des sections affiche un carré blanc sur la section draggé.
- Le drag and drop d'application a quelques soucis
- correctif "GtkWarning: Invalid icon size 64"

- le script du lancement du handymenu-custom crash. (soucis de fermeture d'un if)

## [1.2.6] - 2020-01-26

### Fixed

- permettre le lancement de l'app si elle n'a pas de logo

## [1.2.5] - 2018-12-11
### Added

- Debug mode

## [1.2.4] - 2018-11-08
### Security

- Ajout d'un dialogue de confirmation avant la réinitialisation

### Fixed

- Des doublons de code non supprimés créaient des warnings dans la console
- Le drag and drop des sections peut avoir un effet indésirable sur les boutons "supprimer une section", "monter/descendre une section".
- Déterminer un nombre max de softs par section (définit en dur à 20) car les handymenus sont extensibles alors que la config non.

### Added

- Uniformisation de l'affichage des boutons de configuration
- Fichier de Changelog
