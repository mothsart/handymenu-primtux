#!/usr/bin/python
# coding: utf8
# utils for handymenu

import sys
import os
from os.path import dirname, join, isfile
import yaml
import io
import traceback
import getpass
import copy

import gettext

maxonglets = 9
iconsize   = 64

class Utils():
    def __init__(self, appname, config_path, verbose):
        """ Define options """
        local_path   = '/usr/share/locale'
        hmdir        = "/usr/share/handymenu"
        home_name = None
        if appname == "mini":
            home_name = "01-mini"
        elif appname == "super":
            home_name = "02-super"
        elif appname == "maxi":
            home_name = "03-maxi"
        elif appname == "prof":
            home_name = "administrateur"
        self.verbose           = verbose
        self.handymenu_path    = dirname(dirname(__file__))
        self.app_path          = join(self.handymenu_path, "handymenu-{}".format(appname))
        self.pixmaps           = "/usr/share/pixmaps"
        self.menuname          = "HandyMenu session {}".format(appname)
        self.defaultconfigfile = '/etc/handymenu/handymenu-{}.default.yaml'.format(appname)
        self.configfile        = '/etc/handymenu/handymenu-{}.yaml'.format(appname)
        self.noclose           = '/home/{0}/.config/handymenu/noclose.conf'.format(home_name)
        self.primtux_icons     = join(hmdir, "icons")
        self.primtuxmenuicon   = '/usr/share/handymenu/icons/primtuxmenu_icon-{0}.png'.format(appname)
        # Si on est dans le cas d'un utilisateur personnalisé
        if appname not in ['mini', 'super', 'maxi', 'prof']:
            home_name  = getpass.getuser()
            custom_path            = '/home/{}/.config/handymenu/'.format(home_name)
            self.defaultconfigfile = '{}.default.yaml'.format(appname)
            self.configfile        = '{}conf.yaml'.format(custom_path)
            self.primtuxmenuicon   = '{}menu.png'.format(custom_path)
            if not isfile(self.primtuxmenuicon):
                self.primtuxmenuicon = '/usr/share/handymenu/icons/primtuxmenu_icon.png'
            self.noclose         = '{}noclose.conf'.format(custom_path)
        if not isfile(self.defaultconfigfile):
            # DEBUG MODE
            print("DEBUG MODE")
            self.defaultconfigfile = join(self.app_path, 'handymenu-{}.default.yaml').format(appname)
            local_path             = join(self.handymenu_path, 'locale')
            self.configfile        = join(self.app_path, 'handymenu-{}.yaml'.format(appname))
            self.noclose           = join(self.app_path, 'handymenu-{}-noclose.yaml'.format(appname))
            hmdir                  = self.app_path
            self.primtux_icons     = join(self.handymenu_path, "icons")
            self.primtuxmenuicon = join(self.primtux_icons, "primtuxmenu_icon-{}.png".format(appname))
            # Si on est dans le cas d'un utilisateur personnalisé (on utilise les conf de la session prof)
            if appname not in ['mini', 'super', 'maxi', 'administrateur']:
                self.app_path          = join(self.handymenu_path, "handymenu-prof")
                self.defaultconfigfile = join(self.app_path, 'handymenu-prof.default.yaml')
                self.configfile        = join(self.app_path, 'handymenu-prof.yaml')
                self.primtuxmenuicon   = join(self.primtux_icons, "primtuxmenu_icon-prof.png")
                self.noclose           = join(self.app_path, 'handymenu-prof-noclose.yaml')
        if config_path:
            self.defaultconfigfile = join(config_path, 'conf.default.yaml')
            self.configfile        = join(config_path, 'conf.yaml')
            self.primtuxmenuicon   = join(config_path, "menu.png")
            self.noclose           = join(config_path, 'noclose.yaml')

        self.configcmd = "python {0} {1} &".format(join(self.handymenu_path, "handymenu-configuration.py"), appname)
        gettext.bindtextdomain('handymenu', local_path)
        gettext.textdomain('handymenu')
        self._ = gettext.gettext
        self.appname = appname

    def is_debug_mode(self):
        for c in self.config:
            if 'config' in c and 'debug' in c['config']:
                return c['config']['debug']
        return False

    def set_default_config(self):
        config = self._load_default_config()
        self.save_config(config)

    def _print_verbose(self, config_path, config_stream):
        if not self.verbose:
            return
        print(
            'fichier de conf : %s\n' % config_path
        )
        yaml.dump(config_stream, sys.stdout)

    def _load_default_config(self):
        with io.open(self.defaultconfigfile, 'r') as stream:
            try:
                self.defaultconfig = yaml.load(stream)
            except:
                traceback.print_exc()
                exit(0)
            self._print_verbose(
                self.defaultconfigfile,
                self.defaultconfig
            )
            return(self.defaultconfig)

    def load_config(self):
        try:
            with io.open(self.configfile, 'r') as stream:
                try:
                    self.config = yaml.load(stream)
                except:
                    traceback.print_exc()
                    self.config = self._load_default_config()
                self._print_verbose(self.configfile, self.config)
                return(self.config)
        except Exception as err:
            if self.verbose:
                print(err)
        self.config = self._load_default_config()
        return self.config

    def save_config(self, conf):
        with io.open(self.configfile, 'w', encoding='utf8') as outfile:
            yaml.safe_dump(
                conf,
                outfile,
                default_flow_style=False,
                allow_unicode=True
            )
    
    def add_config(self, config_file):
        try:
            with io.open(config_file, 'r') as stream:
                try:
                    config = yaml.load(stream)
                except:
                    traceback.print_exc()
                    return
                self._print_verbose(config_file, config)
        except Exception as err:
            if self.verbose:
                print(err)
            return
        actual_config = self.load_config()
        _id = self.greater_id(actual_config)
        for _section in config:
            if self.has_section(actual_config, _section):
                self.add_apps(actual_config, _section, _section['apps'], False)
            else:
                _id = _id + 1
                _section['id'] = _id
                actual_config = self.add_section(actual_config, _section, False)
        self.save_config(actual_config)

    def remove_config(self, config_file):
        try:
            with io.open(config_file, 'r') as stream:
                try:
                    config = yaml.load(stream)
                except:
                    traceback.print_exc()
                    return
                self._print_verbose(config_file, config)
        except Exception as err:
            if self.verbose:
                print(err)
            return
        actual_config = self.load_config()
        for _section in config:   
            apps =  _section['apps']
            actual_config = self.del_apps(actual_config, _section, apps, False)
        
        for _section in copy.copy(actual_config):
            if  _section['apps'] != []:
                continue
            actual_config.remove(_section) 
        self.save_config(actual_config)

    def greater_id(self, config):
        _id = 0
        for _section in config:
            if int(_section['id']) > _id:
                _id = int(_section['id'])
        return _id

    def has_section(self, config, section):
        for _section in config:
            if _section['name'] == section['name']:
                return True
        return False

    def add_section(self, config, section, _save=True):
        config.append(section)
        if _save:
            self.save_config(config)
        return config

    def move_section_of(self, config, section, index):
        inc = 1
        if index < 0:
            inc = -1
            index = abs(index)
        while index > 0:
            self.move_section(config, section, inc)
            index = index - 1

    def move_section(self, config, section, index):
        """move section of +1 or -1
        index = +1  or -1
        """
        toreload=False
        for s in config:
            if s == section:
                idx = config.index(s)
                if index == -1 : # on recule l'application
                    if idx > 0 :
                        config[idx], config[idx-1] = config[idx-1], config[idx]
                elif index == 1 : # on avance l'application
                    if idx < len(config) - 1:
                        config[idx], config[idx+1] = config[idx+1], config[idx]
                self.save_config(config)
                toreload=True
                break
        return(toreload)
    
    def add_apps(self, config, section, apps, _save=True):
        for app in apps:
            config = self.add_app(config, section, app, _save)
        return config

    def add_app(self, config, section, app, _save=True):
        for s in config:
            if s['name'] == section['name']:
                s['apps'].append(app)
                break
        if _save:
            self.save_config(config)
        return config
 
    def del_apps(self, config, section, apps, _save=True):
        for app in apps:
            config = self.del_app(config, section, app, _save)
        return config

    def del_app(self, config, section, app, _save=True):
        for s in config:
            if s['name'] == section['name']:
                try:
                    s['apps'].remove(app)
                except:
                    break
                break
        if _save:
            self.save_config(config)
        return config
    
    def mod_app(self, config, section, app, new):
        for s in config:
            if s == section:
                for a in s['apps']:
                    if a == app:
                        a['name'] = new
                        self.save_config(config)

    def mod_section_icon(self, config, section, newicon):
        for s in config:
            if s == section:
                s['icon'] = newicon
                self.save_config(config)
                return

    def mod_app_icon(self, config, section, app, newicon):
        for s in config:
            if s == section:
                for a in s['apps']:
                    if a == app:
                        a['icon'] = newicon
                        self.save_config(config)
                        return

    def move_apps(self, config, section, new_section):
        for s in config:
            if s == section:
                loc = config.index(s)
                new_section['id'] = section['id']
                new_section['name'] = section['name']
                config[loc] = new_section
                self.save_config(config)
                return
