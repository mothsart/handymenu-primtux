#!/usr/bin/python
# coding: utf8

import os
import sys
import yaml
import pygtk
pygtk.require('2.0')
import gtk
import gettext
from hm_utils import *
from __version__ import *

class Handymenu():
    def close_application(self, widget, event, data=None):
        # tests nécessaires pour que seul clic-gauche et Entrée soient valables
        if event.type == gtk.gdk.BUTTON_RELEASE and \
                event.state & gtk.gdk.BUTTON1_MASK:
                gtk.main_quit()
        if event.type != gtk.gdk.KEY_PRESS:
            return
        if event.keyval == gtk.keysyms.Return:
           gtk.main_quit()

    def configure(self, data=None):
        os.system(self.utils.configcmd)
        gtk.main_quit()

    def exec_app(self, widget, event, data):
        exe = False
        if event.type == gtk.gdk.BUTTON_RELEASE and \
                event.state & gtk.gdk.BUTTON1_MASK:
                exe = True
        elif event.type == gtk.gdk.KEY_PRESS: 
            if event.keyval == gtk.keysyms.Return:
                exe = True
        if not exe:
            return
        appname, icon, cmd= data['name'], data['icon'], data['cmd']
        if self.verbose or self.utils.is_debug_mode():
            os.system("xterm {} &".format(cmd.strip()))
        else:
            os.system("{} &".format(cmd.strip()))
        if self.closeafterrun:
            gtk.main_quit()

    def create_tabs(self):
        if self.config == None:
            return
        self.n_onglets = len(self.config)
        for s in self.config:
            if 'apps' not in s:
                continue
            # Description du bouton
            label_str = ''
            if 'name' in s:
                label_str = s['name']
            label = gtk.Label(label_str)
            # onglet coloré
            label.modify_fg(gtk.STATE_NORMAL, gtk.gdk.color_parse("#41B1FF"))
            n = 0
            if 'apps' in s and s['apps'] != None:
                n = len(s['apps']) # number of apps to show
            if n <= 4: # si peu d'applications, 1 seule ligne
                r = 1
            else:
                r = 2 # two rows

            if n%2==0:  # nombre d'applis pair ou pas
                c = n/r - 1
            else:
                c = n/r 
            if n > 8 :  # beaucoup d'applications : + de 2 lignes
                c = 4
                r = n/c

            if n == 0 : # empty section
                c = 1

            page = gtk.Table(rows=r, columns=c, homogeneous=True)
            page.grab_focus()
            page.set_row_spacings(10)
            page.set_col_spacings(10)

            cur = [0,0]
            if n == 0:
                desc = gtk.Label(self.utils._("This menu is still empty"))
                align = gtk.Alignment(0.5, 0.5, 0, 0)
                align.add(desc)
                self.onglets.append_page(align, label)
                break
            for a in s['apps']:
                appname, icon, cmd, generic = a['name'], a['icon'], a['cmd'], a['generic']
                # image utilisée dans le bouton
                image = gtk.Image()
                image_exist = False
                if icon.endswith(('.png', '.jpg', '.ico')):
                    try:
                        pixbuf = gtk.gdk.pixbuf_new_from_file(icon)
                        scaled_buf = pixbuf.scale_simple(iconsize,iconsize,gtk.gdk.INTERP_BILINEAR)
                        image.set_from_pixbuf(scaled_buf)
                        image_exist = True
                    except:
                        pass
                else:
                    image.set_from_icon_name(icon, gtk.ICON_SIZE_BUTTON)
                    image.set_pixel_size(iconsize)
                    icon_theme = gtk.icon_theme_get_default()
                    icon_info = icon_theme.lookup_icon(icon, gtk.ICON_SIZE_BUTTON, 0)
                    if icon_info != None:
                        image_exist = True
                # nom de l'appli
                bapp = gtk.Button(label=appname)
                bapp.set_image(image)
                # l'image est au dessus du texte
                bapp.set_image_position(gtk.POS_TOP)
                # apparence du bouton
                bapp.set_relief(gtk.RELIEF_NONE)
                bapp.connect("button_release_event", self.exec_app, a)
                bapp.connect("key_press_event", self.exec_app, a)
                # Le bouton survolé change de couleur
                bapp.modify_bg(gtk.STATE_PRELIGHT, gtk.gdk.color_parse("#41B1FF"))
                # Description du bouton
                bulledesc = gtk.Tooltips()
                bulledesc.set_tip(bapp, generic)

                page.attach(bapp, cur[0], cur[0]+1, cur[1], cur[1]+1,\
                    xoptions=gtk.EXPAND|gtk.FILL, yoptions=gtk.EXPAND|gtk.FILL,\
                    xpadding=5, ypadding=5)
                if cur[0] < c:
                    cur[0] +=1
                elif cur[0] == c:
                    cur[0] = 0
                    cur[1] += 1
            # pour centrer
            align = gtk.Alignment(0.5, 0.5, 0, 0)
            align.add(page)

            hbox = gtk.HBox()
            img = gtk.Image()
            if 'icon' in s and s['icon'].endswith(('.png', '.jpg', '.ico')):
                try:
                    pixbuf = gtk.gdk.pixbuf_new_from_file(s['icon'])
                    scaled_buf = pixbuf.scale_simple(iconsize, iconsize, gtk.gdk.INTERP_BILINEAR)
                    img.set_from_pixbuf(scaled_buf)
                    hbox.add(img)
                except:
                    pass
            elif 'icon' in s:
                img.set_from_icon_name(s['icon'], gtk.ICON_SIZE_BUTTON)
                img.set_pixel_size(iconsize)
                hbox.add(img)
            hbox.add(label)
            hbox.show_all()
            self.onglets.append_page(align, hbox)

        self.onglets.set_tab_label_packing(align, False, False, gtk.PACK_START)
        if self.n_onglets > maxonglets: # dyp il aime pas :P
            self.onglets.set_scrollable(True)# dyp y veut pas :P

    def close_after(self, widget):
        self.closeafterrun = widget.get_active()
        try:
            with io.open(self.utils.noclose, 'w', encoding='utf8') as noclose_path:
                yaml.safe_dump(
                    { "close": self.closeafterrun },
                    noclose_path,
                    default_flow_style=False,
                    allow_unicode=True
                )
        except Exception as err:
            print("Soucis d'enregistrement de %s" % self.utils.noclose)
            print(err)

    def make_menu(self):
        """build the menu"""
        # Conteneur principal
        mainbox = gtk.EventBox()
        self.window.add(mainbox)

        vbox = gtk.VBox(False, 2)
        vbox.set_border_width(15)
        mainbox.add(vbox)

        # Logo
        image = gtk.Image()
        image.set_from_file(self.utils.primtuxmenuicon)
        logo = gtk.EventBox()
        logo.add(image)
        bulledesc = gtk.Tooltips()
        bulledesc.set_tip(logo, self.utils._("Thuban/HandyLinux-Tomasi/PrimTux"))

        # Titre
        title = gtk.Label()
        title.set_markup('<span size="32000">Handy-PrimTux  </span>')
        title.set_justify(gtk.JUSTIFY_CENTER)
        linkbox = gtk.EventBox()
        linkbox.add(title)
        bulledesc = gtk.Tooltips()
        bulledesc.set_tip(linkbox, "http://primtux.fr")

        # boutons
        # bouton pour fermer
        closebtn = gtk.Button()
        croix = gtk.Image()
        croix.set_from_stock(gtk.STOCK_CLOSE, gtk.ICON_SIZE_MENU)
        closebtn.set_image(croix)

        closebtn.set_relief(gtk.RELIEF_NONE)
        closebtn.connect("button_release_event", self.close_application)
        closebtn.connect("key_press_event", self.close_application)
        bulledesc = gtk.Tooltips()
        bulledesc.set_tip(closebtn, self.utils._("Close"))

       # fermer ou pas
        closeafterbtn = gtk.CheckButton()
        closeafterbtn.connect("toggled", self.close_after)
        closeafterbtn.set_active(self.closeafterrun)
        bulledesc = gtk.Tooltips()
        bulledesc.set_tip(closeafterbtn, self.utils._("Close after execution"))
        
        # configuration 
        qbtn = gtk.Button()
        image = gtk.Image()
        image.set_from_stock(gtk.STOCK_PREFERENCES, gtk.ICON_SIZE_MENU)
        qbtn.set_image(image)
        qbtn.set_relief(gtk.RELIEF_NONE)
        qbtn.connect_object("clicked", self.configure, None)
        bulledesc = gtk.Tooltips()
        bulledesc.set_tip(qbtn, self.utils._("Configure"))

        # boite à boutons 
        btnbox = gtk.VBox(False, 0)
        btnbox.pack_start(closebtn, True, True, 0)
        if self.utils.appname == "prof":
            btnbox.pack_start(qbtn, True, True, 0)
        else:
            btnbox.pack_start(closeafterbtn, True, True, 0)
        btnbox.show();

        # Boite d'en haut
        topbox = gtk.HBox(False, 0)
        topbox.pack_start(logo, True, True, 0)
        topbox.pack_start(btnbox, False, False, 0)

        vbox.pack_start(topbox, True, True, 0)

        # onglets
        self.onglets = gtk.Notebook()
        self.onglets.set_tab_pos(gtk.POS_TOP)
        self.onglets.set_show_border(False)
        align = gtk.Alignment(0.5, 0.5, 0, 0)
        align.add(self.onglets)
        vbox.pack_start(align, True, True, 0)

        # Boite d'en bas
        bottombox = gtk.HBox(True, 0)
        vbox.pack_start(bottombox, False, False, 0)

        # Catégories
        self.create_tabs()

        self.window.show_all()

    def __init__(self, appname, config_path, verbose):
        self.verbose       = verbose
        self.utils         = Utils(appname, config_path, verbose)
        self.closeafterrun = True
        try:
            with io.open(self.utils.noclose, 'r') as stream:
                self.closeafterrun = yaml.load(stream)["close"]
        except:
            pass
        self.n_onglets = 0 # nombre d'onglets
        self.config = self.utils.load_config()
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", lambda x,y: gtk.main_quit())

        self.window.set_title(self.utils.menuname)
        self.window.set_border_width(1) # pour avoir une bordure noire
        self.window.set_icon_from_file(self.utils.primtuxmenuicon)

        self.window.set_position(gtk.WIN_POS_CENTER)
        self.window.set_resizable(False)
        self.window.set_decorated(False)
        self.window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("black"))

        self.make_menu()
        self.onglets.grab_focus() # pour la gestion au clavier facilitée

def main(appname, config_path, verbose):
    if verbose:
        print('handymenu %s' % version)
    menu = Handymenu(appname, config_path, verbose)
    gtk.main()
    return 0

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
