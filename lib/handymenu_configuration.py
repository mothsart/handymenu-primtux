#!/usr/bin/python
# coding: utf8

"""
Description :
    Configuration du handymenu
"""

import sys
import os
import copy
import pygtk
pygtk.require('2.0')
import gtk
import gettext
import locale

from hm_utils import *
from __version__ import *

def get_info_desktop(desktopfile):
    """return infos from a .desktop file"""
    name, cmd, icon, generic= "", "", "", ""
    nameloc = False
    geneloc = False
    lang = locale.setlocale(locale.LC_ALL, "")[0:2]
    with open(desktopfile,'r') as d:
        df = d.readlines()
    for l in df:
        if generic == "" or geneloc == False:
            if l.startswith('GenericName[{0}]='.format(lang)):
                generic = l.replace(
                    'GenericName[{0}]='.format(lang),''
                ).strip()
                geneloc = True
            elif l.startswith('GenericName='.format(lang)):
                generic = l.replace(
                    'GenericName='.format(lang),''
                ).strip()
        if name == "" or nameloc == False:
            if l.startswith('Name[{0}]='.format(lang)):
                name = l.replace(
                    'Name[{0}]='.format(lang),''
                ).strip()
                nameloc = True
            elif l.startswith('Name='):
                name = l.replace('Name=', '').strip()
        if cmd == "" and l.startswith('Exec='):
            cmd = l.replace('Exec=', '').strip()
            cmd = cmd.split('%')[0].strip()
        if icon == "" and l.startswith('Icon='):
            icon = l.replace('Icon=', '').strip()
            if not os.path.exists(icon):
                icon = os.path.splitext(
                    icon
                )[0]
    return name, cmd, icon, generic


class ConfirmDialog(gtk.Dialog):

    def __init__(self, parent, title, message):
        gtk.Dialog.__init__(
            self, title, parent,
            True,
            (
                gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT,
                gtk.STOCK_OK, gtk.RESPONSE_OK
            )
        )

        self.set_default_size(150, 100)
        box = self.get_content_area()
        if message:
            label = gtk.Label(message)
            box.add(label)
        self.show_all()


class HandymenuConfig():
    TARGET_TYPE_TEXT   = 80
    TARGET_TYPE_PIXMAP = 81
    toCanvas           = [ ( "image/x-xpixmap", 0, TARGET_TYPE_PIXMAP ) ]
    APP_MARGIN         = 25
    APP_SIZE           = 150

    def close_application(self, widget, event, data=None):
        appname = "-" + str(self.utils.appname)
        if not self.utils.appname:
            appname = "-prof"
        os.system(
            "{} --force &".format(
                join(
                    self.utils.app_path,
                    "handymenu" + appname
                )
            )
        )
        gtk.main_quit()
        return False

    def appfinder(self, widget=None, event=None):
        os.system('rox /usr/share/applications &')

    def restart(self, widget=None, event=None):
        page = self.section_list.get_current_page()
        self.config = self.utils.load_config()
        self.window.destroy()
        self.make_menu()
        if page > len(self.config):
            page = 0
        self.section_list.set_current_page(page)

    def back_to_default(self, widget):
        confirm = ConfirmDialog(
            widget,
            "Confirmation de réinitialisation",
            "Souhaitez-vous vraiment revenir à la configuration initiale ?"
            + "\nCette opération est irréversible."
        )
        if confirm.run() == gtk.RESPONSE_OK:
            self.utils.set_default_config()
            self.restart()
        confirm.destroy()

    def add_new_section(self, widget):
        name = widget.get_text().strip()
        if len(name) != 0:
            newsec =  {'name' : name, 'id': "", 'apps': [] }
            self._create_section(newsec)
            nb_pages = self.section_list.get_n_pages()
            new_child = self.section_list.get_nth_page(nb_pages - 1)
            self.section_list.reorder_child(new_child, nb_pages - 2)
            self.section_list.set_current_page(
                self.section_list.get_current_page() + 1
            )
            self.section_list.show_all()
            self.utils.add_section(self.config, newsec)

    def del_section(self, section):
        index_page = self.section_list.get_current_page()
        self.section_list.remove_page(index_page)
        if index_page - 1 > 0:
            self.section_list.set_current_page(index_page - 1)
        self.config.remove(section)
        self.utils.save_config(self.config)

    def on_set_debug_mode(self, widget):
        config = { 'debug': widget.get_active() }
        for c in self.config:
            if 'config' in c:
                c['config'] = config
                self.utils.save_config(self.config)
                return
        self.config.append({'config': config})
        self.utils.save_config(self.config)

    def _hide_show_btn(self, child, nb_pages, current_index, next_index):
        thbox      = self.section_list.get_nth_page(0).get_children()[0].get_children()[0].get_children()[0]
        secondhbox = self.section_list.get_nth_page(1).get_children()[0].get_children()[0].get_children()[0]
        bhbox      = self.section_list.get_nth_page(nb_pages - 2).get_children()[0].get_children()[0].get_children()[0]
        lasthbox   = self.section_list.get_nth_page(nb_pages - 3).get_children()[0].get_children()[0].get_children()[0]
        secondhbox.get_children()[3].set_sensitive(True)
        secondhbox.get_children()[4].set_sensitive(True)
        lasthbox.get_children()[3].set_sensitive(True)
        lasthbox.get_children()[4].set_sensitive(True)
        if current_index != 0 and current_index != nb_pages - 2 and current_index != nb_pages - 1:
            currenthbox = self.section_list.get_nth_page(current_index).get_children()[0].get_children()[0].get_children()[0]
            currenthbox.get_children()[3].set_sensitive(True)
            currenthbox.get_children()[4].set_sensitive(True)
        thbox.get_children()[3].set_sensitive(False)
        thbox.get_children()[4].set_sensitive(True)
        bhbox.get_children()[3].set_sensitive(True)
        bhbox.get_children()[4].set_sensitive(False)

    def move_sec(self, app, index):
        nb_pages = self.section_list.get_n_pages()
        current_index = self.section_list.get_current_page()
        next_index = current_index + index
        if next_index == -1:
            return
        if next_index >= nb_pages - 1:
            return
        if next_index == 0:
            self.section_list.reorder_child(self.section_list.get_nth_page(0), 1)
        else:
            self.section_list.reorder_child(app[1], next_index)
        self.utils.move_section(self.config, app[0], index)
        self._hide_show_btn(app[0], nb_pages, current_index, next_index)

    def on_drag_reorder(self, notebook, child, next_index, app):
        nb_pages      = self.section_list.get_n_pages()
        current_index = notebook.get_current_page()
        nodes         = notebook.get_tab_label(child).children()
        if len(nodes) == 1:
            current_name = nodes[0].get_text()
        else:
            current_name = nodes[1].get_text()
        select_app    = None
        inc           = 0
        config_index  = 0
        for app in self.config:
            if app['name'] == current_name:
                config_index = inc
                select_app = app
                break
            inc = inc + 1
        notebook.reorder_child(self.addbox, -1)
        self._hide_show_btn(child, nb_pages, current_index, next_index)
        self.utils.move_section_of(
            self.config,
            select_app,
            current_index - config_index
        )
 
    def add_item_to_section(self, name, cmd, icon, generic, section):
        app = {'name' : name, 'icon' : icon, 'cmd' : cmd, 'generic' : generic}
        self.utils.add_app(self.config, section, app)
        self.restart()

    def del_item_from_section(self, section, app):
        self.utils.del_app(self.config, section, app)
        self.restart()

    def mod_app_name(self, widget, event, dialog, section, app):
        newname = widget.get_text().strip()
        if len(newname) != 0:
            self.utils.mod_app(self.config, section, app, newname)
            self.restart()
        dialog.destroy()

    def mod_section_icon_dialog(self, section, section_menu, label):
        chooser = gtk.FileChooserDialog(
            title=self.utils._("Choose an icon"),
            action=gtk.FILE_CHOOSER_ACTION_OPEN,
            buttons = (
                gtk.STOCK_CANCEL,
                gtk.RESPONSE_CANCEL,
                gtk.STOCK_OPEN,
                gtk.RESPONSE_OK
            )
        )
        chooser.set_current_folder(self.utils.pixmaps)
        filter = gtk.FileFilter()
        filter.set_name(self.utils._("Images"))
        filter.add_mime_type("image/png")
        filter.add_mime_type("image/jpeg")
        ilter.add_mime_type("image/ico")
        chooser.add_filter(filter)

        response = chooser.run()

        if response == gtk.RESPONSE_CANCEL:
            print(self.utils._('Closed, no files selected'))
            chooser.destroy()
        elif response == gtk.RESPONSE_OK:
            i = chooser.get_filename()
            chooser.destroy()
            self.utils.mod_section_icon(self.config, section, i)
            hbox = gtk.HBox()
            img = gtk.Image()
            pixbuf = gtk.gdk.pixbuf_new_from_file(i)
            scaled_buf = pixbuf.scale_simple(24, 24, gtk.gdk.INTERP_BILINEAR)
            img.set_from_pixbuf(scaled_buf)
            hbox.pack_start(img, False, True, 0)
            hbox.pack_end(gtk.Label(label), True, True, 0)
            hbox.show_all()
            current_index = self.section_list.get_current_page()
            page = self.section_list.get_nth_page(current_index)
            self.section_list.get_nth_page(current_index).parent.set_tab_label(page, hbox)

    def mod_app_icon_dialog(self, widget, event, dialog, section, app):
        chooser = gtk.FileChooserDialog(
            title=self.utils._("Choose an icon"),
            action=gtk.FILE_CHOOSER_ACTION_OPEN,
            buttons = (
                gtk.STOCK_CANCEL,
                gtk.RESPONSE_CANCEL,
                gtk.STOCK_OPEN,
                gtk.RESPONSE_OK
            )
        )
        chooser.set_current_folder(self.utils.pixmaps)
        filter = gtk.FileFilter()
        filter.set_name(self.utils._("Images"))
        filter.add_mime_type("image/png")
        filter.add_mime_type("image/jpeg")
        filter.add_mime_type("image/ico")
        chooser.add_filter(filter)

        response = chooser.run()

        if response == gtk.RESPONSE_CANCEL:
            print(self.utils._('Closed, no files selected'))
            chooser.destroy()
        elif response == gtk.RESPONSE_OK:
            i = chooser.get_filename()
            chooser.destroy()
            self.utils.mod_app_icon(self.config, section, app, i)
            self.restart()

        dialog.destroy()

    def on_drag_data_received(self, widget, context, x, y, selection, target_type, timestamp, section):
        data = selection.data.strip().replace('%20', ' ') # On retire le retour à la ligne et le code des espaces
        if data.startswith('file://'):
            f = data.replace("file://", "").strip()
            if os.path.isdir(f): # parse directories
                name = os.path.basename(f)
                cmd = 'exo-open --launch FileManager "{}"'.format(f)
                icon = "folder"
                self.add_item_to_section(name, cmd, icon, None, section)
            elif os.path.isfile(f): # is it file?
                if data.endswith('.desktop'):
                    name, cmd, icon, generic = get_info_desktop(f)
                    self.add_item_to_section(name, cmd, icon, generic, section)
                else:
                    name = os.path.basename(f)
                    cmd = 'exo-open "{}"'.format(f)
                    self.add_item_to_section(name, cmd, "empty", None, section) #raccourci pour fichier sans icone
        elif data.startswith("http://") or \
                data.startswith("https://") or \
                data.startswith("ftp://"):  # cas d'une url
            name = data.split('/')[2]
            cmd = "exo-open --launch WebBrowser {}".format(data)
            self.add_item_to_section(name, cmd, "text-html", "Lien vers une url", section) 

        widget.destroy()

    def add_appli(self, section):
        w = gtk.Dialog(parent=self.window)
        w.connect("delete_event", self.restart)
        w.set_size_request(360, 150)
        TARGET_TYPE_URI_LIST = 80
        dnd_list = [ ( 'text/uri-list', 0, TARGET_TYPE_URI_LIST ) ]
        w.drag_dest_set( gtk.DEST_DEFAULT_MOTION |
                  gtk.DEST_DEFAULT_HIGHLIGHT | gtk.DEST_DEFAULT_DROP,
                  dnd_list, gtk.gdk.ACTION_COPY)

        w.connect("drag_data_received", self.on_drag_data_received, section)
        l = gtk.Label(self.utils._("Drag an icon here to create a launcher"))
        w.vbox.pack_start(l, True, True, 10)

        appfinderbtn = gtk.Button(label=self.utils._("Search for applications"))
        appfinderbtn.connect("button_press_event", self.appfinder)
        w.action_area.pack_start(appfinderbtn, True, True, 0)
        appfinderbtn.show()
        l.show()

        ret = w.run()
        if ret == gtk.RESPONSE_DELETE_EVENT:
            w.destroy()
    
    def del_appli(self, widget, dialog, section, app):
        self.del_item_from_section(section, app)
        dialog.destroy() # delete parent

    def edit_appli(self, widget, event, section, app):
        if event.type != gtk.gdk._2BUTTON_PRESS:
            return
        d = gtk.Dialog(title=self.utils._("Edit the launcher"))
        # Edition du nom de l'appli
        entry = gtk.Entry()
        entry.connect("activate", self.mod_app_name, entry, d, section, app) # entrée valide
        entry.show()

        namebtn = gtk.Button(label = self.utils._("Change"))
        namebtn.connect_object("clicked", self.mod_app_name, entry, None, d, section, app)
        namebtn.show()

        # on met ça dans une boîte
        box = gtk.HBox(False,2)
        box.pack_start(entry, True, True, 3)
        box.pack_start(namebtn, False, False, 0)
        box.show()
        
        # et le tout dans un étiquette
        nameframe = gtk.Frame(self.utils._("Change the label"))
        nameframe.add(box)
        nameframe.show()

        icon = app["icon"]
        iconpreview = gtk.Image()
        if icon.endswith(('.png', '.jpg', '.ico')):
            pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(icon, iconsize, iconsize)
            scaled_buf = pixbuf.scale_simple(iconsize, iconsize, gtk.gdk.INTERP_BILINEAR)
            iconpreview.set_from_pixbuf(scaled_buf)
        else:
            print(icon)
            iconpreview.set_from_icon_name(icon, iconsize)
            iconpreview.set_pixel_size(iconsize)
        iconpreview.show()

        # Changement de l'icône
        iconbtn = gtk.Button(label = self.utils._("Change icon"))
        chgi = gtk.Image()
        chgi.set_from_stock(gtk.STOCK_EDIT, gtk.ICON_SIZE_MENU)
        iconbtn.set_image(chgi)
        iconbtn.connect_object("clicked", self.mod_app_icon_dialog, entry, None, d, section, app)
        iconbtn.show()

        # on met ça dans une boîte
        
        # et le tout dans un étiquette
        iconframe = gtk.Frame(self.utils._("Change the application icon"))
        iconframe.add(iconbtn)
        iconframe.show()

        # Nécessaire pour la suppression
        delbtn = gtk.Button(label = self.utils._("Delete"), stock = gtk.STOCK_DELETE)
        delbtn.connect("clicked", self.del_appli, d, section, app)
        delbtn.show()
        delframe = gtk.Frame(self.utils._("Delete this launcher"))
        delframe.add(delbtn)
        delframe.show()

        # ajout des objets au dialogue
        d.vbox.pack_start(nameframe)
        hbox = gtk.HBox(False, 2)
        hbox.pack_start(iconpreview)
        hbox.pack_start(iconframe)
        hbox.show();
        d.vbox.pack_start(hbox, True, True, 0)

        d.vbox.pack_start(delframe)
        d.run()

    def _create_section(self, s):
        if 'name' not in s:
            return
        nb_apps = len(s['apps'])
        label   = gtk.Label(s['name'])
        hbox    = gtk.HBox()

        img = gtk.Image()
        if 'icon' in s and s['icon'].endswith(('.png', '.jpg', '.ico')):
            try:
                pixbuf = gtk.gdk.pixbuf_new_from_file(s['icon'])
                scaled_buf = pixbuf.scale_simple(24, 24, gtk.gdk.INTERP_BILINEAR)
                img.set_from_pixbuf(scaled_buf)
            except:
                pass
            hbox.pack_start(img, False, True, 0)
        elif 'icon' in s:
            img.set_from_icon_name(s['icon'], gtk.ICON_SIZE_BUTTON)
            img.set_pixel_size(24)
            hbox.pack_start(img, False, True, 0)
        hbox.pack_end(label, True, True, 0)

        hbox.show_all()

        applist = gtk.VBox()
        scrolled_window = gtk.ScrolledWindow()
        scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
        scrolled_window.set_size_request(700, -1)
        self.section_list.append_page(scrolled_window, hbox)
        self.section_list.set_tab_reorderable(scrolled_window, True)

        # boutons de config
        hb = gtk.HBox(False, 10)
        addbtn = gtk.Button(self.utils._("Add an application"))
        addi = gtk.Image()
        addi.set_from_stock(gtk.STOCK_ADD, gtk.ICON_SIZE_MENU)
        addbtn.set_image(addi)
        addbtn.connect_object("clicked", self.add_appli, s)
        if nb_apps >= 20:
            bulledesc = gtk.Tooltips()
            bulledesc.set_tip(
                addbtn,
                "Le nombre d'applications "
                + "par section est limité à %s" % nb_apps
            )
            addbtn.set_sensitive(False)
        else:
            addbtn.set_sensitive(True)

        chiconbtn = gtk.Button(self.utils._("Change icon"))
        chgi = gtk.Image()
        chgi.set_from_stock(gtk.STOCK_EDIT, gtk.ICON_SIZE_MENU)
        chiconbtn.set_image(chgi)
        chiconbtn.connect_object(
            "clicked", self.mod_section_icon_dialog, s, hbox, s['name']
        )

        delbtn = gtk.Button(self.utils._("Delete this section"))
        deli = gtk.Image()
        deli.set_from_stock(gtk.STOCK_DELETE, gtk.ICON_SIZE_MENU)
        delbtn.set_image(deli)
        delbtn.connect_object("clicked", self.del_section, s)

        upbtn = gtk.Button(self.utils._("Move section up"))
        upi = gtk.Image()
        upi.set_from_stock(gtk.STOCK_GO_UP, gtk.ICON_SIZE_MENU)
        upbtn.set_image(upi)
        upbtn.connect_object(
            "clicked",
            self.move_sec,
            (s, scrolled_window),
            -1
        )
        downbtn = gtk.Button(self.utils._("Move section down"))
        downi = gtk.Image()
        downi.set_from_stock(gtk.STOCK_GO_DOWN, gtk.ICON_SIZE_MENU)
        downbtn.set_image(downi)
        downbtn.connect_object(
            "clicked",
            self.move_sec,
            (s, scrolled_window), 
            +1
        )

        hb.pack_start(addbtn)
        hb.pack_start(chiconbtn)
        hb.pack_start(delbtn)
        hb.pack_start(upbtn)
        hb.pack_start(downbtn)
        if s in self.config and self.config.index(s) == 0:
            upbtn.set_sensitive(False)
        if s in self.config and self.config.index(s) == len(self.config) - 1:
            downbtn.set_sensitive(False)
        applist.pack_start(hb, False,False, 10)
        n = 0
        if 'apps' in s and s['apps'] != None:
            n = len(s['apps']) # number of apps to show
        if n <= 4: # si peu d'applications, 1 seule ligne
            r = 1
        else:
            r = 2 # two rows
        c = 4
        if n > 8 :  # beaucoup d'applications : + de 2 lignes
            c = 4
            r = n/c
        if n == 0 : # empty section
            c = 1
        layout = gtk.Layout()
        layout.show()
        layout.set_size(500, 100)
        layout.connect('drag-drop', self.on_drag_drop, s)
        layout.connect('drag-motion', self.on_drag_motion)
        
        layout.drag_dest_set(
            gtk.DEST_DEFAULT_MOTION,
            self.toCanvas, 
            gtk.gdk.ACTION_COPY
        )
        cur = [0,0]
        if n == 0:
            applist.pack_start(layout)
            scrolled_window.add_with_viewport(applist)
            return
        for a in s['apps']:
            appname, icon, cmd= a['name'], a['icon'], a['cmd']
            image = gtk.Image()
            if (
                os.path.isfile(icon)
                and icon.endswith(('.png', '.jpg', '.ico'))
            ):
                pixbuf = gtk.gdk.pixbuf_new_from_file_at_size(icon, iconsize, iconsize)
                scaled_buf = pixbuf.scale_simple(iconsize, iconsize, gtk.gdk.INTERP_BILINEAR)
                image.set_from_pixbuf(scaled_buf)
            else:
                image.set_from_icon_name(icon, iconsize)
                image.set_pixel_size(iconsize)
            # nom de l'appli
            tooltip = gtk.Tooltips()
            bapp = gtk.Button(label=appname)
            tooltip.set_tip(bapp, "double-cliquer pour éditer")
            bapp.set_image(image)
            #l'image est au dessus du texte
            bapp.set_image_position(gtk.POS_TOP)
            # apparence du bouton
            bapp.set_relief(gtk.RELIEF_NONE)
            bapp.connect("button-press-event", self.edit_appli, s, a)
            TARGET_TYPE_TEXT = 80
            TARGET_TYPE_PIXMAP = 81
            bapp.drag_source_set(
                gtk.gdk.BUTTON1_MASK,
                [ ( "text/plain", 0, TARGET_TYPE_TEXT ),
              ( "image/x-xpixmap", 0, TARGET_TYPE_PIXMAP ) ],
                gtk.gdk.ACTION_COPY
            )
            bapp.show_all()
            layout.put(
                bapp, 
                int(self.APP_MARGIN + cur[0] * self.APP_SIZE),
                int(self.APP_MARGIN + cur[1] * self.APP_SIZE)
            )
            if cur[0] < c:
                cur[0] +=1
            elif cur[0] == c:
                cur[0] = 0
                cur[1] += 1
        applist.pack_start(layout)
        scrolled_window.add_with_viewport(applist)

    def on_drag_motion(self, layout, context, x, y, timestamp):
        x_after_drag  = x / self.APP_SIZE
        y_after_drag  = y / self.APP_SIZE
        children      = layout.get_children()
        max_line      = int(len(children) / 5)
        hadj          = layout.get_hadjustment()
        vadj          = layout.get_vadjustment()
        last_x        = 0
        last_y        = 0
        r             = None
        before_list   = []
        new_list      = []
        after_list    = []
        is_after      = False
        for child in children:
            rec     = child.get_allocation()
            child_x = rec.x / self.APP_SIZE
            child_y = rec.y / self.APP_SIZE
            if child == context.get_source_widget():
                drag_child = child
                r = child.get_allocation()
                is_after = True
            else:
                if is_after:
                    after_list.append(child)
                else:
                    before_list.append(child)
            if child_y > last_y:
                last_y = child_y
        if not r:
            return
        x_before_drag = r.x / self.APP_SIZE
        y_before_drag = r.y / self.APP_SIZE
        before_greater = False
        if y_before_drag > y_after_drag:
            before_greater = True
        if y_before_drag == y_after_drag and x_before_drag > x_after_drag:
            before_greater = True

        min_x = x_before_drag
        max_x = x_after_drag
        min_y = y_before_drag
        max_y = y_after_drag
        if before_greater:
            min_x = x_after_drag
            max_x = x_before_drag
            min_y = y_after_drag
            max_y = y_before_drag
        for child in children:
            r       = child.get_allocation()
            child_x = r.x / self.APP_SIZE
            child_y = r.y / self.APP_SIZE
            if child != context.get_source_widget():
                self._move_app(layout, child, child_x, child_y, min_x, min_y, max_x, max_y, before_greater, hadj, vadj)
            if child_y == last_y and child_x > last_x:
                last_x = child_x

        if x_after_drag > 4:
            x_after_drag = 4
        if y_after_drag == last_y and x_after_drag == 4:
            y_after_drag = last_y
            x_after_drag = last_x  
        elif y_after_drag > last_y:
            y_after_drag = last_y
            x_after_drag = last_x
            if last_x == 4:
                y_after_drag = last_y
                x_after_drag = last_x
        layout.move(
            drag_child,
            int(self.APP_MARGIN + x_after_drag * self.APP_SIZE + hadj.value), 
            int(self.APP_MARGIN + y_after_drag * self.APP_SIZE + vadj.value)
        )
        self.drag_index = y_after_drag * 5 + x_after_drag

    def on_drag_drop(self, layout, context, x, y, time, section):
        new_list = copy.copy(layout.get_children())
        before_drag_index = layout.get_children().index(context.get_source_widget())
        new_list.insert(self.drag_index, new_list.pop(before_drag_index))
        new_cycle = iter(copy.copy(new_list))
        new_section = { 'apps': [] }
        while True:
            try:
                label = next(new_cycle)
            except:
                break
            for app in section['apps']:
                if app['name'] == label.get_label():
                    new_section['apps'].append(app)
                    new_list.remove(label)
        self.utils.move_apps(self.config, section, new_section)

    def _move_app(self, layout, child, child_x, child_y, min_x, min_y, max_x, max_y, before_greater, hadj, vadj):
        inc_x   = -1
        if before_greater:
            inc_x = 1
        if min_y == max_y and child_y == min_y and child_x >= min_x and child_x <= max_x:
            layout.move(
                child,
                int(self.APP_MARGIN + (child_x + inc_x) * self.APP_SIZE + hadj.value), 
                int(self.APP_MARGIN + child_y * self.APP_SIZE + vadj.value)
            )
        if min_y == max_y:
            return
        if (
            child_y == min_y and child_x >= min_x
            or child_y > min_y and child_y < max_y
            or child_y == max_y and child_x <= max_x
        ):
            inc_y = 0
            if not before_greater and child_x == 0:
                child_x = 4
                inc_x   = 0
                inc_y   = -1
            if before_greater and child_x == 4:
                child_x = 0
                inc_x   = 0
                inc_y   = 1
            layout.move(
                child,
                int(self.APP_MARGIN + (child_x + inc_x) * self.APP_SIZE + hadj.value), 
                int(self.APP_MARGIN + (child_y + inc_y) * self.APP_SIZE + vadj.value)
            )

    def make_entrylist(self):
        self.section_list = gtk.Notebook()
        self.section_list.set_tab_pos(gtk.POS_LEFT)
        for s in self.config:
            self._create_section(s)

        addlabel = gtk.Image()
        addlabel.set_from_stock(gtk.STOCK_ADD, gtk.ICON_SIZE_MENU)
        bulledesc = gtk.Tooltips()
        bulledesc.set_tip(addlabel, self.utils._("Add a section"))
        self.addbox = gtk.VBox()
        instruction = gtk.Label(self.utils._(
            "Name of the new section: "
        ))
        entry = gtk.Entry()
        entry.connect("activate", self.add_new_section)
        self.addbox.pack_start(instruction, False, True, 3)
        self.addbox.pack_start(entry, False, False, 20)

        addbtn = gtk.Button(self.utils._("Add a section"))
        addi = gtk.Image()
        addi.set_from_stock(gtk.STOCK_ADD, gtk.ICON_SIZE_MENU)
        addbtn.set_image(addi)
        addbtn.connect_object("clicked", self.add_new_section, entry)
        self.addbox.pack_start(addbtn, False, False, 10)
        self.section_list.append_page(self.addbox, addlabel)

        searchtab = self.section_list.get_nth_page(0)
        self.section_list.connect(
            'page-reordered',
            self.on_drag_reorder,
            searchtab
        )
        self.mainbox.pack_start(self.section_list)

    def make_menu(self):
        """build the menu"""
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.connect("delete_event", self.close_application)

        self.window.set_title(
            "Configuration du handymenu : session {}".format(
                self.utils.appname
            )
        )
        self.window.set_border_width(0)

        # Conteneur principal
        self.mainbox = gtk.VBox(False, 10)
        self.mainbox.set_border_width(10)

        # configuration principale
        self.make_entrylist()

        # conteneur pour les boutons
        btnbox = gtk.HBox(True, 2)
        self.mainbox.pack_start(btnbox, False, False, 0)

        debug_check = gtk.CheckButton("Debug")
        debug_check.connect(
            'toggled',
            self.on_set_debug_mode
        )
        debug_check.set_active(self.utils.is_debug_mode())
        btnbox.pack_start(debug_check, fill= False)

        defaultbtn = gtk.Button(label = self.utils._("Reset"))
        resetimg = gtk.Image()
        resetimg.set_from_stock(gtk.STOCK_REDO, gtk.ICON_SIZE_BUTTON)
        defaultbtn.set_image(resetimg)
        defaultbtn.connect_object(
            "clicked", self.back_to_default, self.window
        )
        btnbox.pack_start(defaultbtn, fill= False)

        savebtn = gtk.Button(
            label = self.utils._("Quit"),
            stock=gtk.STOCK_CLOSE
        )
        savebtn.connect_object(
            "clicked",
            self.close_application,
            self.window, None
        )
        btnbox.pack_start(savebtn, fill= False)

        self.window.add(self.mainbox)
        self.window.set_default_size(1000, 730)
        self.window.show_all()

    def __init__(self, appname, config_path, verbose):
        self.utils = Utils(appname, config_path, verbose)
        self.config = self.utils.load_config()
        self.make_menu()

def main(appname, config_path, verbose):
    if verbose:
        print('handymenu %s' % version)
    menu = HandymenuConfig(appname, config_path, verbose)
    gtk.main()
    return 0

# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4
